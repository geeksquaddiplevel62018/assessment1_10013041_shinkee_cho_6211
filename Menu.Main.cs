using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    partial class Program
    {
        /// <summary>
        /// Displays main menu on the screen.
        /// </summary>
        private static void DisplayMainMenu()
        {
            string strInputNum;
            Dictionary<int,string> menuList = new Dictionary<int, string>();

            // Menu list            
            menuList.Add(1,"Queue");
            menuList.Add(2,"Palindrome");
            menuList.Add(3,"Infix and Postfix");

            Console.Clear();

            do
            {
                Console.WriteLine(PrintMenu(menuList, true));

                Console.Write(" > Enter one of the number above : ");
                strInputNum = Console.ReadLine();

                switch(strInputNum)
                {
                    case "1" :
                        DisplayMyQueueMenu();        // Refer to file Menu.Queue.cs
                        break;

                    case "2" :
                        DisplayPalindromeMenu();   // Refer to file Menu.Palindrome.cs
                        break;

                    case "3" :
                        DisplayInfixPostfixMenu();// Refer to file Menu.InfixAndPostfix.cs
                        break;

                    case "0" :
                        Console.WriteLine(" > Thank you!");
                        break;

                    default :
                        ShowError(ERROR_CODE.WRONG_MENU_NUM);
                        break;
                }

                Console.Clear();

            } while(strInputNum != "0");
        }
    }
}
