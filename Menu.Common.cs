using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// Part of Program class for common methods for menu
    /// </summary>
    partial class Program
    {
        /// <summary>
        /// Defines numbers of error codes.
        /// </summary>
        private enum ERROR_CODE
        {
          WRONG_MENU_NUM,
          EMPTY_STRING,
          WRONG_EQUATION
        };

        /// <summary>
        /// Adds a new menu of printing the array
        /// </summary>
        /// <param name="menuList">String array contained menus' texts</param>
        /// <param name="menuText">New menu text</param>
        /// <returns></returns>
        private static string AddMenu(Dictionary<int,string> menuList, string menuText)
        {
            menuList.Add(menuList.Count+1, menuText);
            return PrintMenu(menuList);
        }

        /// <summary>
        /// Shows error message.
        /// </summary>
        /// <param name="code">Registred error code</param>
        private static void ShowError(ERROR_CODE code)
        {
            Console.WriteLine();

            switch(code)
            {
                case ERROR_CODE.WRONG_MENU_NUM :
                    Console.WriteLine(" > Error : Please, type in right number.");
                    break;
                
                case ERROR_CODE.WRONG_EQUATION :
                    Console.WriteLine(" > Error : The equation typed in is something wrong. Please check it again.");
                    break;

                case ERROR_CODE.EMPTY_STRING :
                    Console.WriteLine(" > Error : Please, type in a string.");
                    break;

                default:
                    Console.WriteLine();
                    Console.WriteLine(" > This error code is not exist.");
                    break;
            }
        }

        /// <summary>
        /// Puts end message and clears screen.
        /// </summary>
        private static void PutEndCommand()
        {
            Console.WriteLine();
            Console.WriteLine(" > Press any key.");
            Console.ReadKey();
            Console.Clear();
        }

        /// <summary>
        /// Displays menu by decided design.
        /// </summary>
        /// <param name="list">Menu list</param>
        /// <param name="isMainPage">For main menu or not. Default value is false.</param>
        /// <returns></returns>
        private static string PrintMenu(Dictionary<int, string> list, bool isMainPage = false)
        {
            string menu = string.Empty;

            menu += " ----------------------------------\n";

            foreach(KeyValuePair<int, string> kv in list)
            {
                menu += "    " + kv.Key + ". " + kv.Value + '\n';
            }

            menu += "\n";
            menu += isMainPage ? "    0. exit\n" : "    0. Go to Main Page\n";
            menu += " ----------------------------------\n";

            return menu;
        }

        /// <summary>
        /// Removes white space in string.
        /// </summary>
        /// <param name="str">String to remove white space</param>
        /// <returns>A string removed withe space</returns>
        public static string RemoveWhitespace(string str)
        {
            string newStr = string.Empty;

            foreach(char c in str)
            {
                if (!char.IsWhiteSpace(c))
                {
                    newStr += c;
                }
            }
            
            return newStr;
        }

        /// <summary>
        /// Clears screen after seconds
        /// </summary>
        /// <param name="sec">Duration (unit: second)</param>
        private static void AutoRefresh(int sec = 3)
        {
            Console.WriteLine();
            
            for(int i=sec; i > 0; i--)
            {
                Console.Write("\r   >>> Refresh after 3 second : {0}",i);
                Thread.Sleep(1000);
            }

            Console.Clear();
        }
    }
}