using System;
using System.Collections;

namespace assessment1_10013041_sing
{
    partial class Program
    {
        /// <summary>
        /// Displays palindrom menu on the screen
        /// </summary>
        private static void DisplayPalindromeMenu()
        {            
            string input;

            // Print menu page on console
            Console.WriteLine();
            Console.WriteLine(" > If you want to quit, press enter key. ");
            Console.Write(" > Type in any string to check if it is a palindrome : ");
            input =  Console.ReadLine();

            // Breaks when pressing enter key.
            if(string.IsNullOrWhiteSpace(input)){ return; }

            Console.WriteLine();
            
            // Checks if palindrome.
            if(input.IsPalindrome() == true)
            {
                Console.WriteLine("  [Result] Yes, it is a palindrome");
            }
            else
            {
                Console.WriteLine("  [Result] No, it is not a palindrome");
            }

            PutEndCommand();
        }
    }
}