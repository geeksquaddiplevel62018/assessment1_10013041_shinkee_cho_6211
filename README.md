# Assesment 1 : COMP6211 'Algorithms and Data Structures'
* This is for an assessment1 of __COMP6211 'Algorithms and Data Structures'__ in Toi-Ohomai.

## 1. Information
* **Student Name** : Shinkee Cho
* **Student ID**   : 10013041

## 2. What files is for the question of this assessment? ###

>### 2-1. Question1 : Writing Queue Implementation

>>Requirement          |  File Name      | Line number | Note
>>:----------          |:-----------     |:-----------:|:----
>>__Adding directive__ | Menu.MyQueue.cs |  12         | DisplayMyQueueMenu()
>>__MyQueue__ Class    | MyQueue.cs      |  37         | Myqueue class
>>__PrintQueue__ Method| MyQueue.cs      |  57         | PrintQueue()
>>__Enqueue__ Method   | MyQueue.cs      |  87         | Enqueue()
>>__Dequeue__ Method   | MyQueue.cs      |  96         | Dequeue()
>>__Contains__ Method  | MyQueue.cs      |  106        | Contains()
>>__ToArray__ Method   | MyQueue.cs      |  115        | ToArray()

>### 2-2. Question2 : Palindromes

>>Requirement                                     |  File Name         | Line number | Note
>>:-----------------------------------------------|:-------------      |:-----------:|:-----
>>__Adding directive__                            | Menu.Palindrome.cs |  11         | DisplayPalindromeMenu()
>>Ignore capitalization, spaces and punctuation   | Palindrome.cs      |  72         | PushLetters()
>>Implementing stack routine                      | Palindrome.cs      |  112        | CheckPalindrome()

>>**I have used string extension instead of a palindrome class to implement this requirement.**
>>**Because, the document does not say that a class with name of Palindrome is necessary,**
>>**and I just thought string extension is better than the class.**

>### 2-3. Question3 : Evaluating Expressions with a Stack

>>Requirement                           |  File Name                 | Line number | Note
>>:------------------------------------ |:-----------                |:-----------:|:----
>>__Adding directive__                  | Menu.InfixAndPostfix.cs    |  12         | DisplayInfixPostfixMenu()
>>__InfixToPostfixConverter__ Class     | InfixToPostfixConverter.cs |  57         | InfixToPostfixConverter class
>>|||This class is **derived** from **InfixAndPostfix class** in InfixAndPostfix.cs
>>__IsOperator__ Method                 | InfixToPostfixConverter.cs |  150        | IsOperator()
>>__Precedence__ Method                 | InfixToPostfixConverter.cs |  209        | Precedence()
>>__ConvertToPostfix__ Method           | InfixToPostfixConverter.cs |  79         | ConvertToPostfix()

>### 2-4. Question4 : Evaluating a Postfix Expression with a Stack

>>Requirement                           |  File Name              | Line number | Note
>>:------------------------------------ |:-----------             |:-----------:|:----
>>__Adding directive__                  | Menu.InfixAndPostfix.cs |  12         | DisplayInfixPostfixMenu()
>>__PostfixEvaluator__ Class            | PostfixEvaluator.cs     |  61         | PostfixEvaluator class
>>__EvaluatePostfixExpression__ Method  | PostfixEvaluator.cs     |  73         | EvaluatePostfixExpression ()
>>__Calculate__ Method                  | PostfixEvaluator.cs     |  138        | Calculate()

>### 2-5. Extra Classes and Methods

>>Class / Method                     |  File Name                 | Purpose
>>:--------------------------------- |:-----------                |:-----------
>>__PostfixToInfixConverter__ Class  | PostfixToInfixConverter.cs | To evaluate by converting postfix to infix. 
>>__Menu.Main.cs__ file              | Menu.Common.cs             | Shows main menu and operates.
>>__Menu.MyQueue.cs__ file           | Menu.MyQueue.cs            | Shows MyQueue menu and operates.
>>__Menu.Palindrome.cs__ file        | Menu.Palindrome.cs         | Shows Palindrome menu and operates.
>>__Menu.InfixAndPostfix.cs__ file   | Menu.InfixAndPostfix.cs    | Shows Infix and Postfix menu and operates.
>>__Menu.Common.cs__ file            | Menu.Common.cs             | There are common methods using in menu operating.

