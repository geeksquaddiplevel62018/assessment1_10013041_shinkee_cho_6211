using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    partial class Program
    {
        /// <summary>
        /// Displays infix and postfix menu on the screen.
        /// </summary>
        private static void DisplayInfixPostfixMenu()
        {
            string infix;

            // Prints menu page on console
            Console.WriteLine();
            Console.WriteLine(" > ---------------------------");
            Console.WriteLine(" >  Operators : ^, *, /, %, +, -");
            Console.WriteLine(" >  Equation example : (6+2)^(2/5)*4-(4+2)%3*3");
            Console.WriteLine(" > ---------------------------");
            Console.WriteLine(" >  Result :");
            Console.WriteLine(" >  1. Infix expression typed in.");
            Console.WriteLine(" >  2. Postfix expression converted from the infix and the calculation result");
            Console.WriteLine(" >  3. Infix expression reconverted from the postfix.");
            Console.WriteLine(" > ---------------------------");
            

            //-----------------------
            // Receives an equation by a user
            //-----------------------
            Console.WriteLine();
            Console.Write(" > Type in an equation : ");
            infix =  Console.ReadLine();

            // Removes characters if they are not number, operator and parenthesis.
            //-----------------------
            infix = RemoveWhitespace(infix);
            
            // Checks errors
            if(infix == string.Empty)
            {
                ShowError(ERROR_CODE.EMPTY_STRING);
                PutEndCommand();
                return;
            }


            //-----------------------
            // Result
            //-----------------------
            Console.WriteLine();
            Console.WriteLine(" > [Result]");

            // 1. Infix expression and calculation result of the infix.
            //-----------------------
            Console.WriteLine(" > Infix                           : {0}", infix);


            // 2. Postfix expression converted from the infix and the calculation result.
            //-----------------------
            InfixToPostfixConverter i2p = new InfixToPostfixConverter();
            string postfix = i2p.ConvertToPostfix(infix);
            
            // Checks errors
            if(postfix == null)
            {
                ShowError(ERROR_CODE.WRONG_EQUATION);
                PutEndCommand();
                return;
            }

            // Evaluates the postfix.
            PostfixEvaluator postfixEvaluator = new PostfixEvaluator();
            double? calculationResult = postfixEvaluator.EvaluatePostfixExpression(postfix);
            
            Console.WriteLine(" > Postfix                         : {0} = {1}", postfix, calculationResult);


            // 3. Infix expression converted from the postfix.
            //-----------------------
            PostfixToInfixConverter p2i = new PostfixToInfixConverter();
            string infixToConfirm = p2i.ConvertToInfix(postfix);
            Console.WriteLine(" > Infix converted from the posfix : {0}", infixToConfirm);


            PutEndCommand();
        }
    }
}