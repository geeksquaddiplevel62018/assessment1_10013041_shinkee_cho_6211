using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// This class contains common methods for any classes to implement infix or postfix.
    /// </summary>
    class InfixAndPostfix
    {
        /// <summary>
        /// Decides priority of operator, and returns the number of the priority
        /// </summary>
        /// <param name="s">String type operator : It should be contained one charactor</param>
        /// <returns>The number of the priority. If not operator, return -1.</returns>
        protected int Priority(string s){ return Priority(s[0]); }
        
        /// <summary>
        /// Decides priority of operator, and returns the number of the priority
        /// </summary>
        /// <param name="c">Charactor type operator</param>
        /// <returns>The number of the priority. If not operator, return -1.</returns>
        protected int Priority(char c)
        {
            switch(c)
            {
                case '^' : return 3;
                case '*' : return 2;
                case '/' : return 2;
                case '%' : return 2;
                case '+' : return 1;
                case '-' : return 1;
                default : return -1;
            }
        }

        /// <summary>
        /// Decides priority of operator, and returns the number of the priority when converting postfix to infix.
        /// </summary>
        /// <param name="s">String type operator : It should be contained one charactor</param>
        /// <returns>The number of the priority. If not operator, return -1.</returns>
        protected int RightPriority(string s){ return RightPriority(s[0]); }
        
        /// <summary>
        /// Decides priority of operator, and returns the number of the priority when converting postfix to infix.
        /// </summary>
        /// <param name="c">Charactor type operator</param>
        /// <returns>The number of the priority. If not operator, return -1.</returns>
        protected int RightPriority(char c)
        {
            switch(c)
            {
                case '^' : return 6;
                case '%' : return 5;
                case '/' : return 4;
                case '*' : return 3;
                case '-' : return 2;
                case '+' : return 1;
                default : return -1;
            }
        }
    }
}