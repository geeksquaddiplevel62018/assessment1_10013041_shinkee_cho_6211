/***********************************************************************
 * This is an assessment1 of COMP6211 'Algorithms and Data Structures'
 * on 1st semester in Toi-Ohomai.
 * 
 * Author Info
 * ------------
 * Name       : Shinkee Cho (Sing)
 * Student ID : 10013041
 * Email      : 10013041@student.toiohomai.ac.nz
 * 
 * Question 1 : Writing Queue Implementation (35 marks) 
 * ------------
 * Queue is a commonly known data structure.
 * A queue is similar to a checkout line in a supermarket
 * — the cashier services the person at the beginning of the line first.
 * Other customers enter the line only at the end and wait for service.
 * Queue nodes are removed only from the head (or front) of the queue
 * and are inserted only at the tail (or end).
 * For this reason, a queue is a first-in, first-out (FIFO) data structure.
 * You are required to write a generic class (“MyQueue”) that will have following methods: 
 * 
 * printQueue   : Displays the contents of the Queue 
 * Enqueue      : Adds an object to the end of the Queue 
 * Dequeue      : Removes and returns the object at the beginning of the Queue 
 * Contains     : Determines whether an element is in the Queue 
 * ToArray      : Copies the Queue to a new array and display the resulting array
 ***********************************************************************/

using System;
using System.Collections;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// Class MyQueue
    /// </summary>
    class MyQueue
    {
        /// <summary>
        /// Queue instant 
        /// </summary>
        private Queue _queue;

        /// <summary>
        /// Constructor
        /// </summary>
        public MyQueue()
        {
            _queue = new Queue();
        }

        /// <summary>
        /// Display the content of a queue by string
        /// </summary>
        /// <param name="delimiter"></param>
        /// <returns>String with values of a queue1</returns>
        public string PrintQueue(string delimiter = ",")
        {
            return PrintQueue("","",delimiter);
        }

        /// <summary>
        /// Display the content of a queue by string
        /// </summary>
        /// <param name="prefix">Prefix to add in front of each value of a queue</param>
        /// <param name="subfix">Subfix to add in end of each value of a queue</param>
        /// <param name="delimiter">A valuse to add between each valus of a queue</param>
        /// <returns>String with values of a queue</returns>
        public string PrintQueue(string prefix, string subfix, string delimiter = ",")
        {
            string strQueueValues = string.Empty;
            int cnt = _queue.Count, i = 1;

            foreach(object obj in _queue)
            {
                if(cnt == i++) { delimiter = ""; }
                strQueueValues += prefix + obj.ToString() + subfix + delimiter;
            }

            return strQueueValues;
        }

        /// <summary>
        /// Adds an object to the end of the Queue  
        /// </summary>
        /// <param name="obj">Object to enqueue</param>
        public void Enqueue(object obj)
        {
            _queue.Enqueue(obj);
        }
 
        /// <summary>
        /// Removes and returns the object at the beginning of the Queue 
        /// </summary>
        /// <returns>Object dequeued</returns>
        public object Dequeue()
        {
            return _queue.Dequeue();
        }

        /// <summary>
        /// Determines whether an element is in the Queue 
        /// </summary>
        /// <param name="obj">Object for checking if it is contained.</param>
        /// <returns>Whether the argument(object) is contained in queue.</returns>
        public bool Contains(object obj)
        {
            return _queue.Contains(obj);
        }

        /// <summary>
        /// Copies the Queue to a new array and display the resulting array
        /// </summary>
        /// <returns>Array of object</returns>
        public object[] ToArray()
        {
            return _queue.ToArray();
        }

        /// <summary>
        /// Return the first value enqueued, and removes it from the queue.
        /// </summary>
        /// <returns>First value of the queue</returns>
        public object Peek()
        {
            return _queue.Peek();
        }
    }
}