/***********************************************************************
 * This is an assessment1 of COMP6211 'Algorithms and Data Structures'
 * on 1st semester in Toi-Ohomai.
 * 
 * Author Info
 * ------------
 * Name       : Shinkee Cho (Sing)
 * Student ID : 10013041
 * Email      : 10013041@student.toiohomai.ac.nz
 * 
 * Question 3 : Evaluating Expressions with a Stack (20 marks) 
 * ------------
 * (6 + 2) * 5 - 8 / 4 to a postfix expression.
 * The postfix version of the preceding infix expression is 6 2 + 5 * 8 4 / - 
 * The algorithm for creating a postfix expression is as follows: 
 * a)	Push a left parenthesis '(' on the stack. 
 * b)	Append a right parenthesis ')' to the end of infix. 
 * c)	While the stack is not empty, read infix from left to right and do the following: 
 *      If the current character in infix is a digit, append it to postfix. 
 *      If the current character in infix is a left parenthesis, push it onto the stack. 
 *      If the current character in infix is an operator: 
 *          Pop operators (if there are any) at the top of the stack while they have equal or higher precedence
 *          than the current operator, and append the popped operators to postfix. 
 *          Push the current character in infix onto the stack. 
 *      If the current character in infix is a right parenthesis: 
 *          Pop operators from the top of the stack and append them to postfix until a left parenthesis is at the top of the stack. 
 *          Pop (and discard) the left parenthesis from the stack. d)
 * End 
 * 
 * Operators :
 * 
 * + addition 
 * - subtraction 
 * * multiplication 
 * / division 
 * ^ exponentiation 
 * % modulus 
 * 
 * Some of the methods you may want to provide in your program follow: 
 * a)	Method ConvertToPostfix, which converts the infix expression to postfix notation. 
 * b)	Method IsOperator, which determines whether c is an operator. 
 * c)	Method Precedence, which determines whether the precedence of operator1 (from the infix expression) is less than,
 *      equal to or greater than the precedence of operator2 (from the stack).
 *      The method returns true if operator1 has lower precedence than or equal precedence to operator2.
 *      Otherwise, false is returned. 
 ***********************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// Converts infix expression to postfix expression.
    /// </summary>
    class InfixToPostfixConverter : InfixAndPostfix
    {
        /// <summary>
        /// Stack instant using while converting
        /// </summary>
        private Stack _stack;

        /// <summary>
        /// Constructor
        /// </summary>
        public InfixToPostfixConverter()
        {
            // Creates Stack instant
            _stack = new Stack();
        }


        /// <summary>
        /// Converts infix string to postfix string.
        /// </summary>
        /// <param name="infix">Infix expression string</param>
        /// <returns>Postfix expression string</returns>
        public string ConvertToPostfix(string infix)
        {
            String postfix = "";

            // Clear the stack
            _stack.Clear();

            // a) Push a left parenthesis '(' on the stack.
            _stack.Push('(');

            // b) Append a right parenthesis ')' to the end of infix. 
            infix += ')';

            // c) While the stack is not empty, read infix from left to right and do the following:
            foreach(char c in infix)
            {
                // c-1) If the current character in infix is a digit, append it to postfix.
                if(IsNumber(c)) { postfix += c; }

                // c-2) If the current character in infix is a left parenthesis, push it onto the stack
                else if(IsLeftParenthesis(c)){ _stack.Push(c); }

                // c-3) If the current character in infix is an operator: 
                else if(IsOperator(c))
                {
                    // c-3-1)
                    // Pop operators (if there are any) at the top of the stack
                    // while they have equal or higher precedence than the current operator,
                    // and append the popped operators to postfix
                    //
                    // The precedence of current operator <= one of the top of the stack
                    while(Precedence(c, (char)_stack.Peek()))
                    {
                        postfix += _stack.Pop();
                    }

                    // c-3-2) Push the current character in infix onto the stack.
                    _stack.Push(c);
                }
                
                // c-4) If the current character in infix is a right parenthesis: 
                else if(IsRightParenthesis(c))
                {
                    // c-4-1) 
                    // Pop operators from the top of the stack and append them to postfix
                    // until a left parenthesis is at the top of the stack. 
                    // Pop (and discard) the left parenthesis from the stack. d) 
                    while((char)_stack.Peek() != '(')
                    {
                        postfix += _stack.Pop();
                    }

                    // Pop '(' from the stack.
                    _stack.Pop();
                }

                // Error : A wrong character
                else
                {
                    return null;
                }
            }

            return postfix;
        }

        /// <summary>
        /// Check if operator.
        /// </summary>
        /// <param name="c">Character input</param>
        /// <returns>If the charator is operator, returns true. If not, returns false</returns>
        private bool IsOperator(char c)
        {
            switch(c)
            {
                case '^' :
                case '*' :
                case '/' :
                case '%' :
                case '+' :
                case '-' :
                    return true;
                default:
                    return false;
            }            
        }

        /// <summary>
        /// Check if number.
        /// </summary>
        /// <param name="c">Character input</param>
        /// <returns>If the charator is number, returns true. If not, returns false</returns>
        private bool IsNumber(char c)
        {
            int ascii = Convert.ToInt32(c);
            return (48 <= ascii && ascii <= 57);
        }

        /// <summary>
        /// Check if left parenthesis.
        /// </summary>
        /// <param name="c">Character input</param>
        /// <returns>If the charator is left parenthesis, returns true. If not, returns false</returns>
        private bool IsLeftParenthesis(char c)
        {
            return (c == '(');
        }

        /// <summary>
        /// Check if right parenthesis.
        /// </summary>
        /// <param name="c">Character input</param>
        /// <returns>If the charator is right parenthesis, returns true. If not, returns false</returns>
        private bool IsRightParenthesis(char c)
        {
            return (c == ')');
        }


        /// <summary>
        /// c) Method Precedence, which determines whether the precedence of operator1 (from the infix expression)
        /// is less than, equal to or greater than the precedence of operator2 (from the stack).
        /// The method returns true if operator1 has lower precedence than or equal precedence to operator2.
        /// Otherwise, false is returned. 
        ///
        /// The precedence of operators : '^' > '*', '/', '%' > '+', '-'
        /// </summary>
        /// <param name="operator1">Operator 1 from infix expression</param>
        /// <param name="operator2">Operator 2 from stack</param>
        /// <returns></returns>
        private bool Precedence(char operator1, char operator2)
        {
            return (Priority(operator1) <= Priority(operator2));
        }
    }
}