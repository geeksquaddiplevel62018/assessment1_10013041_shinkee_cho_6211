/***********************************************************************
 * This is an assessment1 of COMP6211 'Algorithms and Data Structures'
 * on 1st semester in Toi-Ohomai.
 * 
 * Author Info
 * ------------
 * Name       : Shinkee Cho (Sing)
 * Student ID : 10013041
 * Email      : 10013041@student.toiohomai.ac.nz
 * 
 * Converts postfix to infix.
 * Postfix '6 2 + 5 * 8 4 / -' to a infix '* (6 + 2) * 5 - 8 / 4' expression.
 * 
 * Explain the algorithm of GroupPiecesOfInfix()
 * ------------
 * a) While there are input symbol left.
 *    Read the next symbol from input.
 * b) If the symbol is an operator.
 *      b-1) If there are fewer than 2 values on the stack
 *           The user has not input sufficient values in the expression.(Error)
 *      b-2) Else, Pop the top 2 values from the stack,
 *           and then push them and the operator as an object to the stack again.(operand1, operator, operand 2).
 * c) Otherwise, the symbol is not operator(i.e. value or object)
 *    Push it onto the stack.
 * d) The last value in the stack is the combination of infix expression.
 *    Combine them to make infix string.
 *   
 * The result is like a tree struct.
 *  (s)   : string
 *  obj   : object
 *  l     : left operand
 *  op    : operator
 *  r     : right operand
 * 
 *            obj (root)
 *      {l,   op,    r}
 *       |    (s)    |
 *       |           |
 *      obj         obj 
 *   {l,  op,  r}   {l, op, r}
 *    |   (s)  |    (s) (s) |
 *    |        |            |
 *   obj      obj          obj
 * {l,op,r}  {l,op,r}     {l,op,r}
 * (s)(s)(s) (s)(s)(s)    (s)(s)(s)
 * 
 * Explain the algorithm of CombinePiecesOfInfix()
 * ------------
 * The item input index to root object of the tree above.
 * It could be a string(raw data), a struct object with {left:, operator:, right:} fields.
 * The fields left and right of the struct object can contain another struct object as the same type.
 * This algorithm travels the tree from the root and reaches the end of each branch.
 * 
 * a) Input an item as the result of above process GroupPiecesOfInfix().
 * 
 * b) If the item is raw data (string itself), return it.
 *    (This means that the trip of tree reached the end of the branch.)
 * 
 * c) Otherwise, the item is a struct object.
 *    Go to step 'a' with left operand of the item to input, and right operand again.
 *    (It is recursive to travel the tree struct.)
 *    That is, if the return value is not string,
 *    this statment in this method recursived is going to recursively call this method itself again.
 *
 *      d) If left operand of current item is a struct object,
 *          d-1) If the operator priority of current item is great than the operator priority of left operand,
 *               or if the operator in current item and left operand of current item are the same as '^',
 *               left operand will be in parenthsis.
 *               * Priority : '^'(3) > '*'(2) > '/'(2) > '%'(2) > '+'(1) > '-'(1)
 *               * Greater number is high priority.
 *
 *      e) If right operand of current item is a struct object,
 *          e-1) If the operator priority of current item is great than the operator priority of right operand of the item,
 *               or the operator in current item and right operand of current item are the same as '-' or '/',
 *               right operand will be in parenthsis.
 *               * Priority : '^'(6) > '%'(5) > '/'(4) > '*'(3) > '-'(2) > '+'(1)
 *               * Greater number is high priority.
 * 
 * f) Combines the strings of left operand, operator and right operand.
 *    This string is an infix expression string.
 * 
 ***********************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// Converts postfix to infix
    /// </summary>
    class PostfixToInfixConverter : InfixAndPostfix
    {
        /// <summary>
        /// Stack instant using while converting
        /// </summary>
        private Stack _stack;

        /// <summary>
        /// Data structure to push into stack
        /// </summary>
        private struct LeftOperatorRight
        {
            public object left;         // Left operand
            public object right;        // Right operand
            public string @operator;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public PostfixToInfixConverter()
        {
            // Create an instance of stack.
            _stack = new Stack();
        }

        /// <summary>
        /// Converts postfix string to infix string.
        /// -----------------------
        /// Assumes that postfix expression is valid and one digit number is used.
        /// 
        /// * Infix                      : 4-(5-8)*6
        /// * Postfix                    : 458-6*-
        /// * Grouping infix expressions : {left:'4', operator:'-', right:{left:{left:'5', operator:'-', right:'8'}, operator:'*', right:'6'}}
        ///   - Step1 : A => {left:'5', operator:'-', right:'8'} => '5 - 8'
        ///   - Step2 : B => {left:A, operator:'*', right:'6'}   => 'A * 6'
        ///   - Step3 : C => {left:'4', operator:'-', right:B}   => '4 - B'
        /// 
        ///* Combination of the groups  : 
        ///   - Step1 : 5 - 8
        ///   - Step2 : 5 - 8  *  6
        ///             -----  -  -
        ///             left   op right
        /// 
        ///   - Step3 : Current item's operator('*') has higer priority than one of left operand('-').
        ///   - Step4 : (5 - 8) * 6
        ///   - Step5 : 4 - (5 - 8) * 6
        ///             - -  ----------
        ///          left op right
        ///   - Step6 : Current item's operator('-') has lower priority than one of right operand('*').
        ///             So, there is not parenthesis.
        /// 
        /// </summary>
        /// <param name="postfix">Postfix expression string</param>
        /// <returns>Infix expression string</returns>
        public string ConvertToInfix(string postfix)
        {
            _stack.Clear();

            // Groups the pieces of infix expression.
            object groupOfpiecesOfInfix = GroupPiecesOfInfix(postfix);

            // Combines the group of pieces of infix expression to make infix string,
            // then returns the string.
            return CombinePiecesOfInfix(groupOfpiecesOfInfix);
        }

        /// <summary>
        /// Separates postfix into groups with operands and an operator by using struct LeftOperatorRight.
        /// The basic structure is a simple infix expression.
        /// For example, x + y => (x, +, y)
        /// </summary>
        /// <param name="postfix">Postfix expression to convert into infix</param>
        /// <returns>Object of a tree structure cotaining struct LeftOperatorRight</returns>
        private object GroupPiecesOfInfix(string postfix)
        {
            // a)
            // While there are input symbol left.
            // Read the next symbol from input.
            foreach(char c in postfix)
            {
                switch(c)
                {
                    // b) If the symbol is an operator.
                    case '^' :
                    case '*' :
                    case '/' :
                    case '%' :
                    case '+' :
                    case '-' :

                        // b-1)
                        // If there are fewer than 2 values on the stack
                        // The user has not input sufficient values in the expression.(Error)
                        if(_stack.Count < 2){ return "Invalid Expression"; }

                        // b-2)
                        // Else, Pop the top 2 values from the stack,
                        // and then push them and the operator to the stack again.(operand1, operator, operand 2).
                        _stack.Push(new LeftOperatorRight{ right=_stack.Pop(), @operator=c.ToString(), left=_stack.Pop() } );

                        break;

                    // c)
                    // Otherwise, the symbol is an operand (i.e. value)
                    // Push it onto the stack.
                    default: 
                        _stack.Push(c.ToString());
                        break;
                }
            }

            // d) The last value in the stack is the combination of infix expression.
            return _stack.Pop();
        }

        /// <summary>
        /// This method combines pieces of infix expresson pushed in the stack as struct LeftOperatorRight,
        /// then returns an infix string with parenthesises.
        /// The struct LeftOperatorRight has three fields as two operand(left, right) and one operator.
        /// The important thing is that parenthesises is used as few as possible,
        /// and this method is a recursive method. 
        /// </summary>
        /// <param name="item">It may be a operand(left-side or right-side number) or an operator string, or an instance of struct LeftOperatorRight.</param>
        /// <returns>Infix string</returns>
        private string CombinePiecesOfInfix(object item)
        {
            string strLeftOperand = null, strRightOperand = null;

            // a)
            // If this item is raw data (string itself), return the value of the item.
            // (In this case, raw data means string, not LeftOperatorRight struct.
            // So, it does not have operands and operator both once.)
		 	if(item is string)
            {
                return (string)item;
            }

            // b)
            // Otherwise, the item is LeftOperatorRight struct,
            // (That is, this item have left and right operand, and an operator.
            // Also, operand can be an instance of LeftOperatorRight struct because this method is recursive.)
            else if(item is LeftOperatorRight)
            {
                // Casting
                LeftOperatorRight currentItem = (LeftOperatorRight)item;
                
                // b-1)
                // Calls this method recursively with left operand and right operand to get the raw data(string).
                // (These operands can be string or instance of LeftOperatorRight struct)
                //
                // That is, if the return value is not string,
                // this statment in this method recursived is going to recursively call this method itself again.
                strLeftOperand = CombinePiecesOfInfix(currentItem.left);
                strRightOperand = CombinePiecesOfInfix(currentItem.right);

                // b-2) If left operand of current item is not string but an instance of LeftOperatorRight struct,
                if(!(currentItem.left is string) && (currentItem.left is LeftOperatorRight))
                {
                    // Casting
                    LeftOperatorRight leftOfItem = (LeftOperatorRight)currentItem.left;

                    // b-2-1)
                    // If the operator priority of current item is great than the operator priority of left operand of the item,
                    // or the operator in current item and left operand of current item are the same as '^',
                    // left operand will be in parenthsis.
                    // 
                    // *** Using Priority() method for priorities of operators
                    //
                    // ex) current item = {left:{left:'5', operator:'-', right:'8'}, operator:'*', right:6} 
                    //                    -----------------------------------------  ------------  -------
                    //                     {left, operator, right} of left operand     operator     right
                    //
                    //                     ==> (5 - 8) * 6
                    //
                    // or current item = {left:{left:'5', operator:'^', right:'8'}, operator:'^', right:6}
                    //                   -----------------------------------------  ------------  -------
                    //                    {left, operator, right} of left operand     operator     right
                    //
                    //                     ==> (5 ^ 8) ^ 6
                    //
                    if(Priority(leftOfItem.@operator) < Priority(currentItem.@operator)
                        || (leftOfItem.@operator == currentItem.@operator && currentItem.@operator == "^"))
                    {
                        strLeftOperand = '(' + (string)strLeftOperand + ')';
                    }
                }

                // b-3) If right operand of current item is not string but an instance of LeftOperatorRight struct,
                if(!(currentItem.right is string) && (currentItem.right is LeftOperatorRight))
                {
                    // Casting
                    LeftOperatorRight rightOfItem = (LeftOperatorRight)currentItem.right;

                    // b-3-1)
                    // If the operator priority of current item is great than the operator priority of right operand of the item,
                    // or the operator in current item and right operand of current item are the same as '-' or '/',
                    // right operand will be in parenthsis.
                    // 
                    // *** Using RightPriority() method for priorities of operators
                    //
                    // ex)current item = {left:6, operator:'*', right:{left:'5', operator:'-', right:'8'}} 
                    //                   -------- ------------  -----------------------------------------
                    //                     left     operator      {left, operator, right} of right operand
                    //
                    //                    ==> 6 * (5 - 8)
                    //
                    // or current item = {left:6, operator:'-', right:{left:'5', operator:'-', right:'8'}} 
                    //                   -------- ------------  -----------------------------------------
                    //                    left     operator      {left, operator, right} of right operand
                    //
                    //                    ==> 6 - (8 - 6)
                    //
                    if(RightPriority(rightOfItem.@operator) < RightPriority(currentItem.@operator)
                        || (rightOfItem.@operator == currentItem.@operator && (currentItem.@operator == "-") || currentItem.@operator == "/"))
                    {
                        strRightOperand = '(' + (string)strRightOperand + ')';
                    }
                }
            }

            // c) Combines the strings of left operand, operator and right operand.
            return strLeftOperand + ((LeftOperatorRight)item).@operator.ToString() + strRightOperand;
        }
    }
}