using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    partial class Program
    {
        /// <summary>
        /// Displays my queue menu on the screen.
        /// </summary>
        private static void DisplayMyQueueMenu()
        {
            string strInputNum;
            string strMenu;
            MyQueue myQueue = new MyQueue();
            Object[] myQueueArray = null;
            Dictionary<int, string> menuList = new Dictionary<int,string>();

            // Menu list
            menuList.Add(1, "Enqueue");
            menuList.Add(2, "Dequeue");
            menuList.Add(3, "Check a value contained");
            menuList.Add(4, "Convert to an array");
            menuList.Add(5, "Print items of the queue");
            strMenu = PrintMenu(menuList);

            do
            {
                Console.Clear();

                // Print menu page on console
                Console.WriteLine(strMenu);
                Console.Write(" > Enter one of the number above : ");
                strInputNum = Console.ReadLine();

                switch(strInputNum)
                {
                    case "1" :    // Enqueues
                        MenuEnqueue(myQueue);
                        if(myQueueArray != null){ myQueueArray = myQueue.ToArray(); };
                        break;

                    case "2" :    // Dequeues
                        MenuDequeue(myQueue);
                        if(myQueueArray != null){ myQueueArray = myQueue.ToArray(); };
                        break;

                    case "3" :    // Checks if containing a value.
                        MenuContains(myQueue);
                        break;

                    case "4" :    // Converts to array
                        myQueueArray = MenuConvertToArray(myQueue);
                        strMenu = AddMenu(menuList, "Print items of the array");
                        break;

                    case "5" :    // Prints values of the queue
                        MenuPrintQueue(myQueue);
                        break;

                    case "6" :    // Prints values of the array
                        // If there is an array converted from a queue,
                        if(myQueueArray != null){ MenuPrintArray(myQueueArray); }
                        // If not, there is not a menu with number '6'.
                        else { ShowError(ERROR_CODE.WRONG_MENU_NUM); }
                        break;

                    case "0" :    // Go back to main menu
                        return;

                    default :
                        if(string.IsNullOrWhiteSpace(strInputNum)){ continue; }
                        else { ShowError(ERROR_CODE.WRONG_MENU_NUM); }
                        break;
                }

                PutEndCommand();

            } while(strInputNum != "0");
        }

        /// <summary>
        /// Implementation of menu 'Enqueue'
        /// </summary>
        /// <param name="myQueue">Reference of MyQueue's instant</param>
        private static void MenuEnqueue(MyQueue myQueue)
        {
            Console.Write(" > Type in any string to enqueue : ");

            string input = Console.ReadLine();
            
            if(string.IsNullOrWhiteSpace(input))
            {
                Console.WriteLine(" > Please, type in a string, not space or enter key");
            }
            else
            {
                myQueue.Enqueue(input);
                Console.WriteLine();
                Console.WriteLine(" > [Result] '{0}' is enqueued.", input);
                ShowQueueValues(myQueue);
            }
        }

        /// <summary>
        /// Implementation of menu 'Dequeue'
        /// </summary>
        /// <param name="myQueue">Reference of MyQueue's instant</param>
        private static void MenuDequeue(MyQueue myQueue)
        {
            Console.WriteLine();
            Console.WriteLine(" > [Result] '{0}' is dequeued.", myQueue.Dequeue());
            ShowQueueValues(myQueue);
        }

        /// <summary>
        /// Implementation of menu 'Contains'
        /// </summary>
        /// <param name="myQueue">Reference of MyQueue's instant</param>
        private static void MenuContains(MyQueue myQueue)
        {
            Console.Write(" > Type in a string to check if it is contained : ");                        
            
            string input = Console.ReadLine();
            
            Console.WriteLine();
            
            if(myQueue.Contains(input) == true)
            {
                Console.WriteLine(" > [Result] Yes, '{0}' is being contained.", input);
            }
            else
            {
                Console.WriteLine(" > [Result] No, there is not '{0}' in the queue.", input);
            }
        }

        /// <summary>
        /// Implementation of menu 'Convert to array'
        /// </summary>
        /// <param name="myQueue">Reference of MyQueue's instant</param>
        /// <returns></returns>
        private static Object[] MenuConvertToArray(MyQueue myQueue)
        {
            Console.WriteLine();
            Object[] myQueueArray = myQueue.ToArray();

            Console.WriteLine(" > [Result] The queue has been converted to array");
            Console.WriteLine(" > Array List : ");
            ShowArrayValues(myQueueArray);

            return myQueueArray;
        }

        /// <summary>
        /// Implementation of menu 'Print the queue'
        /// </summary>
        /// <param name="myQueue">Reference of MyQueue's instant</param>
        private static void MenuPrintQueue(MyQueue myQueue)
        {
            Console.WriteLine();
            Console.WriteLine(" > [Result]");
            ShowQueueValues(myQueue);
        }

        /// <summary>
        /// Implementation of menu 'Print the array'.
        /// </summary>
        /// <param name="myQueueArray">Reference of array converted from queue</param>
        private static void MenuPrintArray(Object[] myQueueArray)
        {
            Console.WriteLine();
            Console.WriteLine(" > [Result] Values of the array.");
            Console.WriteLine();
            ShowArrayValues(myQueueArray);
        }

        /// <summary>
        /// Display the values of an array
        /// </summary>
        /// <param name="array">Reference of an array to display</param>
        private static void ShowArrayValues(object[] array)
        {
            for(int i=0;i<array.Length;i++)
            {
                Console.WriteLine(" > [{0}] : {1}", i, array[i]);
            }
        }

        /// <summary>
        /// Display the values of a queue.
        /// </summary>
        /// <param name="myQueue">Reference of MyQueue's instant</param>
        private static void ShowQueueValues(MyQueue myQueue)
        {
            Console.Write(" > Queue (Left first in) : ");
            Console.Write(myQueue.PrintQueue(" < "));
        }
    }
}