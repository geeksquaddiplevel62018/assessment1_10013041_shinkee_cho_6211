/***********************************************************************
 * This is an assessment1 of COMP6211 'Algorithms and Data Structures'
 * on 1st semester in Toi-Ohomai.
 * 
 * Author Info
 * ------------
 * Name       : Shinkee Cho (Sing)
 * Student ID : 10013041
 * Email      : 10013041@student.toiohomai.ac.nz
 * 
 * Question 4 : Evaluating a Postfix Expression with a Stack (20 marks)
 * ------------
 * 
 * Write class PostfixEvaluator, which evaluates a postfix expression (assume it is valid)
 * such as 6 2 + 5 * 8 4 / -. <=== (6 + 2) * 5 - 8 / 4 to a postfix expression.
 * 
 * The algorithm (for single-digit numbers) is as follows: 
 * a) Append a right parenthesis ')' to the end of the postfix expression.
 *    When the right parenthesis character is encountered, no further processing is necessary. 
 * b) When the right-parenthesis character has not been encountered,
 *    read the expression from left to right. 
 *      If the current character is a digit, do the following: 
 *          Push its integer value on the stack
 *          (the integer value of a digit character is its value
 *          in the computer’s character set minus the value of '0' in Unicode). 
 *      Otherwise, if the current character is an operator: 
 *          Pop the two top elements of the stack into variables x and y. 
 *          Calculate y operator x. 
 *          Push the result of the calculation onto the stack. 
 * c) When the right parenthesis is encountered in the expression,
 *    pop the top value of the stack. This is the result of the postfix expression. 
 * d) End 
 * 
 * [Note: (Based on the sample expression at the beginning of this question),
 * if the operator is '/', the top of the stack is 4 and the next element in the stack is 8,
 * then pop 4 into x, pop 8 into y, evaluate 8 / 4 and push the result, 2, back on the stack.
 * This note also applies to operator '-'.]
 * 
 * The arithmetic operations allowed in an expression are: 
 * + addition 
 * - subtraction 
 * * multiplication 
 * / division 
 * ^ exponentiation 
 * % modulus 
 * 
 * You may want to provide the following methods: 
 * a) Method EvaluatePostfixExpression, which evaluates the postfix expression. 
 * b) Method Calculate, which evaluates the expression op1 operator op2. 
 ***********************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// Postfix Evaluator Class
    /// </summary>
    class PostfixEvaluator
    {
        /// <summary>
        /// Stack instant using while calculating
        /// </summary>
        Stack _stack;

        /// <summary>
        /// Calculates the postfix expression, return the calculation result.
        /// </summary>
        /// <param name="postfix">Postfix expression string</param>
        /// <returns>Result of calculating the equation of postfix type</returns>
        public double? EvaluatePostfixExpression(string postfix)
        {
            double? result = 0;

            _stack = new Stack();

            // a) Append a right parenthesis ')' to the end of the postfix expression.
            //    When the right parenthesis character is encountered, no further processing is necessary. 
            postfix += ')';

            // b) When the right-parenthesis character has not been encountered,
            //    read the expression from left to right. 
            foreach(char c in postfix)
            {
                switch(Convert.ToInt32(c))
                {
                    // b-1) If the current character is a digit, do the following:     
                    case int n when (48 <= n && n <= 57) : // ASCII code 48 is a character '0', and 57 is a character '9'

                        // b-1-1)
                        // Push its integer value on the stack
                        // (the integer value of a digit character is its value
                        // in the computer’s character set minus the value of '0' in Unicode). 
                        _stack.Push((double)(n-48));
                        break;
                    
                    // b-2) Otherwise, if the current character is an operator:
                    case 37 :   // '%'
                    case 42 :   // '*'
                    case 43 :   // '+'
                    case 45 :   // '-'
                    case 47 :   // '/'
                    case 94 :   // '^'

                        // b-2-1)
                        // Pop the two top elements of the stack into variables x and y. 
                        // Calculate y operator x.
                        result = Calculate((double)_stack.Pop(), c, (double)_stack.Pop());
                        // Check Errors
                        if(result == null){ Console.WriteLine("Calculate() methods returned Error"); }
       
                        // b-2-2) Push the result of the calculation onto the stack. 
                        _stack.Push(result);

                        break;

                    // c) When the right parenthesis is encountered in the expression,
                    case ')' :

                        // c-1) pop the top value of the stack. This is the result of the postfix expression. 
                        result = (double)_stack.Pop();
                        break;
                }
            }

            return result;
        }

        /// <summary>
        /// Calculates the simple equation and return the calculation result.
        /// </summary>
        /// <param name="y">Right operland Y</param>
        /// <param name="op">Operator</param>
        /// <param name="x">Left operland X</param>
        /// <returns>Result of calculation</returns>
        private double? Calculate(double y, char op, double x)
        {
            double? result = null;

            switch(op)
            {
                case '^' :
                    result = Math.Pow(x, y);
                    break;

                case '*' :
                    result = x*y;
                    break;

                case '/' :
                    result = x/y;
                    break;

                case '%' : 
                    result = x%y;
                    break;
                
                case '+' :
                    result = x+y;
                    break;
                
                case '-' :
                    result = x-y;
                    break;
                
                default : 
                    result = null;
                    break;
            }

            return result;
        }
    }
}
        