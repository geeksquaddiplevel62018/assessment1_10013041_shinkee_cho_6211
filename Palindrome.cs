/***********************************************************************
 * This is an assessment1 of COMP6211 'Algorithms and Data Structures'
 * on 1st semester in Toi-Ohomai.
 * 
 * Author Info
 * ------------
 * Name       : Shinkee Cho (Sing)
 * Student ID : 10013041
 * Email      : 10013041@student.toiohomai.ac.nz
 * 
 * Question 2 : Palindromes (25 marks)
 * ------------
 * Write a program that uses a stack to determine whether a string is a palindrome
 * (i.e., the string is spelled identically backward and forward).
 * The program should ignore capitalization, spaces and punctuation.
 ***********************************************************************/

using System;
using System.Collections;

namespace assessment1_10013041_sing
{
    /// <summary>
    /// String Extention Class
    /// </summary>
    public static class StringExtension
    {
        /// <summary>
        /// String Extension Method
        /// -----------------------
        /// Checks if the string is a palindrome, and returns boolean value.
        /// 
        /// Use 1 : 
        /// String str = new String("abcba");
        /// if(str.isPalindrome() == true){}
        /// 
        /// Use 2 :
        /// if("abcba".isPalindrome() == true){}
        /// </summary>
        /// <param name="str"></param>
        /// <returns>whether the string input is palindrome.</returns>
        public static bool IsPalindrome(this String str)
        {
            Stack stack = new Stack();

            // Pushes letters of a string into a stack.
            PushLetters(str, stack);

            // Check if the string is a palendrome.
            return CheckPalindrome(stack);
        }

        /// <summary>
        /// String Extension Method
        /// -----------------------
        /// Pushes letters of the parameter str into the parameter stack
        /// Before pushing the letters, checks each letter and ignores capitalization, whitespace and punctuation. 
        /// 
        /// Use 1 : 
        /// String str = new String("abcba");
        /// Stack stack = str.pushLetters();
        /// or "abcba".pushLetters(stack);
        /// 
        /// Use 2 :
        /// String str = new String("abcba");
        /// Stack stack = new Stack();
        /// str.pushLetters(stack);
        /// or "abcba".pushLetters(stack);
        /// </summary>
        /// <param name="str">String to be pushed into stack as letter</param>
        /// <param name="stack">Stack pushed letters</param>
        public static Stack PushLetters(this String str, Stack stack = null)
        {
            if(stack == null){ stack = new Stack(); }

            // a) Makes lower case letters to ignore capitalization. 
            str.ToLower();

            // b) Pushes each charaters of a string into stack.
            foreach(char c in str)
            {
                // c) Ignores whitespace and punctuation.
                if(Char.IsWhiteSpace(c) || Char.IsPunctuation(c)){ continue; }
                
                // d) Pushes the character.
                stack.Push(c);
            }

            return stack;
        }

        /// <summary>
        /// Checks if it is a palindrome.
        /// -----------------------
        /// Explain the steps : 
        /// 
        /// Step 1               Step 2: remove a middle value   Step 3 : Compares
        /// ------------------   ----------------------------    -------------------
        ///  stack     stack2     Pop stack,     Push stack2
        ///                           +----->>>>-----+
        ///                           |              |
        ///                           |              V
        /// |  a' |   |     |      |     |        |     |
        /// |  b' |   |     |      |     |        |     |
        /// |  c  |   |     |      |     |        |     |
        /// |  b  |   |     |      |  b  |        |  b' |
        /// |  a  |   |     |      |  a  |        |  a' |
        ///  -----     -----        -----          -----
        /// </summary>
        /// <param name="stack">Stack</param>
        /// <returns>If palindrome, true if not, false</returns>
        private static bool CheckPalindrome(Stack stack)
        {
            Stack stack2 = new Stack();
            int stackSize = 0;
            double halfAStackSize = 0.0;
            
            // Finds out stack size, half of stack size.
            stackSize = stack.Count;
            halfAStackSize = (stackSize-1)/2.0;

            // Checks
            for(int i=0; i<stackSize; i++)
            {
                // Latter half of the string, push into stack2
                if(i < halfAStackSize)
                {
                    stack2.Push(stack.Pop());
                }
                // If the stack size is odd number, pop the middle character of stack.
                else if(i == halfAStackSize)
                {
                    stack.Pop();
                }
                // Checks if the first half of the string equals the latter half of the string.
                else    // : i > halfAStackSize
                {
                    if(stack.Pop().Equals(stack2.Pop())){ continue; }
                    else { return false; }
                }
            }

            return true;
        }
    }
}